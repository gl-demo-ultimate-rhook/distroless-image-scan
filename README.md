# distroless-image-scan

Using files from https://github.com/docker-library/hello-world/blob/master/amd64/hello-world/, this repo attempts to replicate an issue scanning distroless dockerfiles.

Also uses docs from [here](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration)

This repo has a `grype` and a `trivy` branch for comparison between the two scanners

